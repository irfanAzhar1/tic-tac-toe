require "minitest/autorun"
require_relative "../../app/models/game"

class GameTest < Minitest::Test
  def setup
    @game = Game.instance
    @game.new_game
  end

  def test_play_move
    @game.play(0)
    assert_equal("X", @game.board[0])
    assert_equal("O", @game.current_player)
  end

  def test_switch_players
    @game.switch_players
    assert_equal("O", @game.current_player)
    @game.switch_players
    assert_equal("X", @game.current_player)
  end

  def test_previous_player
    @game.switch_players
    assert_equal("X", @game.previous_player)
    @game.switch_players
    assert_equal("O", @game.previous_player)
  end

  def test_over?
    refute(@game.over?)
    @game.board = ["X", "X", "X", "O", "O", " ", " ", " ", " "]
    assert(@game.over?)
  end

  def test_outcome
    @game.board = ["X", "X", "X", "O", "O", " ", " ", " ", " "]
    assert_equal("#{@game.player1_name} wins!", @game.outcome)
    @game.board = ["O", "O", "O", "X", "X", " ", " ", " ", " "]
    assert_equal("#{@game.player2_name} wins!", @game.outcome)
    @game.board = ["X", "O", "X", "O", "X", "O", "O", "X", "O"]
    assert_equal("It's a draw!", @game.outcome)
    @game.board = ["X", " ", "X", "O", "O", " ", " ", " ", " "]
    assert_nil(@game.outcome)
  end

  def test_set_player_names
    @game.set_player_names("Alice", "Bob")
    assert_equal("Alice", @game.player1_name)
    assert_equal("Bob", @game.player2_name)
  end

  def test_reset
    @game.play(0)
    Game.reset
    assert_equal([" ", " ", " ", " ", " ", " ", " ", " ", " "], @game.board)
    assert_equal("X", @game.current_player)
  end

  def test_play_invalid_move
    @game.play(0)
    assert_raises(StandardError) { @game.play(0) }
  end

  def test_play_invalid_position
    assert_raises(StandardError) { @game.play(100) }
  end

  def test_current_player_name
    @game.set_player_names("Alice", "Bob")
    assert_equal("Alice", @game.current_player_name)
    @game.switch_players
    assert_equal("Bob", @game.current_player_name)
  end

  def test_play_move_json
    result = @game.play(0)
    assert_equal("O", result[:current_player])
    assert_equal("O", @game.current_player)
  end

  def test_winner
    @game.board = ["X", "X", "X", "O", "O", " ", " ", " ", " "]
    assert_equal(@game.player1_name, @game.winner)
    @game.board = ["O", "O", "O", "X", "X", " ", " ", " ", " "]
    assert_equal(@game.player2_name, @game.winner)
    @game.board = ["X", "O", "X", "O", "X", "O", "O", "X", "O"]
    assert_nil(@game.winner)
  end

  def test_draw
    @game.board = ["X", "O", "X", "O", "X", "O", "O", "X", "O"]
    assert(@game.draw?)
    @game.board = ["X", "O", "X", "O", "X", "O", "O", "X", " "]
    refute(@game.draw?)
  end

  def test_singleton
    game1 = Game.instance
    game2 = Game.instance
    assert_same(game1, game2)
  end


end
