# Tic Tac Toe Rails Application

This README provides an overview of the Tic Tac Toe Rails application, including its purpose, how to set it up, and how to play the game.

## Overview

The Tic Tac Toe Rails application is a simple web-based game of Tic Tac Toe. It features a single-page application where two players can take turns to play the game. The game logic is encapsulated in a `Game` class, which is a singleton, ensuring there is only one instance of the game at any time. The game state is managed on the server side, and the client interacts with the game through a web interface.

## Features

- **Single-page Application**: The game is played within a single page, providing a seamless user experience.
- **Real-time Game State**: The game state is managed on the server, ensuring that the game state is always up-to-date and consistent across all clients.
- **Player Names**: Players can set their names before starting the game.
- **Game Reset**: Players can reset the game at any time, allowing for multiple games to be played in a single session.

## Getting Started

### Prerequisites

- Ruby 3.1.2 or later
- Rails 6.0 or later

### Installation

1. Clone the repository:

   ```
   git clone https://gitlab.com/irfanAzhar1/tic-tac-toe.git
   ```

2. Navigate to the project directory:

   ```
   cd tic-tac-toe-rails
   ```

3. Install the Ruby dependencies:

   ```
   bundle install
   ```

4. Start the Rails server:

   ```
   rails server
   ```

5. Open your web browser and navigate to `http://localhost:3000` to start playing the game.

## How to Play

1. **Set Player Names**: Before starting the game, click on the "Set Player Names" button to enter the names of the two players.

2. **Play the Game**: Click on any of the empty squares on the game board to place your mark. The game alternates between the two players.

3. **Check for a Winner**: The game automatically checks for a winner after each move. If a player wins, a message will be displayed, and the game will be reset.

4. **Reset the Game**: If you wish to start a new game, click on the "Reset Game" button.