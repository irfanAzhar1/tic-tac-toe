# This class represents the game logic for a Tic Tac Toe game.
# It has a board, a current player, and methods to play a move, check if the game is over, and determine the winner.
# The class is a singleton, meaning there can only be one instance of it in the application.
# The instance method returns the current instance of the game or creates a new one if it doesn't exist.
# The reset method resets the current instance of the game.
# The play method takes a position as an argument and updates the board with the current player's move.
# It also switches the current player and returns a hash with the game state.
# The over? method checks if the game is over by checking if there is a winner or if the board is full.
# The winner method determines the winner by checking the winning combinations on the board.
# The winning_combinations method defines the possible winning combinations in the game.
# The draw? method checks if the game is a draw by checking if the game is over and there is no winner.
# The outcome method returns the outcome of the game based on the winner or draw status.
# The new_game method resets the board and sets the current player to "X".
# The Game class is used in the controller to handle the game logic and update the view based on the game state.
class Game
  attr_accessor :board, :current_player, :player1, :player2, :player1_name, :player2_name
  @@instance = nil

  def self.instance
    return @@instance ||= Game.new
  end

  def set_player_names(player1, player2)
    @player1_name = player1
    @player2_name = player2
  end

  def self.reset
    @@instance.new_game
  end

  def board
    @board
  end

  def current_player
    @current_player
  end

  def current_player_name
    @current_player == "X" ? @player1_name : @player2_name
  end

  def play(position)
    raise "Invalid move" if @board[position] != " "
    @board[position] = @current_player
    switch_players

    return {
      over: over?,
      outcome: outcome,
      current_player: @current_player,
      current_player_name: current_player_name,
      previous_player: previous_player,
      board: @board,
      position: position
    }
  end

  def previous_player
    @current_player == "X" ? "O" : "X"
  end
  def switch_players
    if @current_player == "X"
      @current_player = "O"
    else
      @current_player = "X"
    end
  end

  def over?
    winner.present? || @board.all? { |cell| cell != " " }
  end

  def winner
    winning_combinations.each do |combo|
      if combo.all? { |cell| cell == "X" }
        return @player1_name
      elsif combo.all? { |cell| cell == "O" }
        return @player2_name
      end
    end
    nil
  end

  def winning_combinations
    [
      # rows
      [@board[0], @board[1], @board[2]],
      [@board[3], @board[4], @board[5]],
      [@board[6], @board[7], @board[8]],
      # columns
      [@board[0], @board[3], @board[6]],
      [@board[1], @board[4], @board[7]],
      [@board[2], @board[5], @board[8]],
      # diagonals
      [@board[0], @board[4], @board[8]],
      [@board[2], @board[4], @board[6]]
    ]
  end

  def draw?
    over? && !winner
  end

  def outcome
    return "#{winner} wins!" if winner
    return "It's a draw!" if draw?
    return nil
  end

  def new_game
    @board = Array.new(9, " ")
    @current_player = "X"
  end

  private

  def initialize
    @player1 = "X"
    @player2 = "O"
    @player1_name = "Player 1"
    @player2_name = "Player 2"
    @board = Array.new(9, " ")
    @current_player = @player1
  end
end
