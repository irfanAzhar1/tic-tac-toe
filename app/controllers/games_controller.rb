class GamesController < ApplicationController
  before_action :set_game

  # GET /games or /games.json
  def index
  end

  # POST /games/reset
  def reset
    Game.reset
    render :index
  end

  def set_player_names
    @game.set_player_names(player_names_params[:player1], player_names_params[:player2])
    render :index
  end

  def update_board
    begin
      result = @game.play(params[:position].to_i)
      return render json: result
    rescue StandardError => e
      return render json: { error: e.message }, status: :bad_request
    end
  end

  private

  def player_names_params
    params.permit(:player1, :player2)
  end

  def set_game
    @game = Game.instance
  end
end
