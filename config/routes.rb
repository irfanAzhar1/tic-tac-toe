Rails.application.routes.draw do
  root "games#index"
  resources :games, only: %i[index] do
    collection do
      post :reset
      patch :update_board
      post :set_player_names
    end
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
